#!/usr/bin/env bash
sudo ansible-playbook create_user_and_dir.yaml \
		 -i inventories/dev/hosts \
		 -c local \
		 "$@"

