#!/usr/bin/env bash
sudo ansible-playbook wordpress.yaml \
                 -i inventories/dev/hosts \
                 -c local \
                 "$@"

